package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/galiherlangga/recipes-api/controllers"
	"gitlab.com/galiherlangga/recipes-api/middlewares"
	"gitlab.com/galiherlangga/recipes-api/models"
)

func main() {
	models.ConnectDatabase()

	r := gin.Default()

	public := r.Group("/api")
	public.POST("/register", controllers.Register)
	public.POST("/login", controllers.Login)

	client := r.Group("/api/client")
	client.Use(middlewares.JwtAuthMiddleware())
	client.GET("/user", controllers.CurrentUser)
	client.GET("/recipes", controllers.RecipesIndex)
	client.GET("/recipe/:id", controllers.RecipesShow)
	client.POST("/recipe", controllers.RecipesCreate)
	client.PUT("/recipe/:id", controllers.RecipesUpdate)

	r.Run(":8004")
}
