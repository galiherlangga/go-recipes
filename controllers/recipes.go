package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/galiherlangga/recipes-api/models"
	"gitlab.com/galiherlangga/recipes-api/utils/token"
	"gorm.io/gorm"
)

type CreateInput struct {
	Name string `json:"name" binding:"required"`
}

func RecipesIndex(c *gin.Context) {
	var recipes []models.Recipes

	models.DB.Preload("User").Find(&recipes)
	c.JSON(http.StatusOK, gin.H{"recipes": recipes})
}

func RecipesShow(c *gin.Context) {
	var recipes models.Recipes
	id := c.Param("id")

	if err := models.DB.Preload("User").First(&recipes, id).Error; err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Recipe not found"})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{"recipe": recipes})
}

func RecipesCreate(c *gin.Context) {
	var input CreateInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user_id, err := token.ExtractTokenID(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	recipes := models.Recipes{}
	recipes.Name = input.Name
	recipes.UserID = uint(user_id)

	_, saveErr := recipes.SaveRecipes()
	if saveErr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "recipes has been saved"})

}

func RecipesUpdate(c *gin.Context) {
	var input CreateInput
	var recipes models.Recipes

	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	recipe_id := c.Param("id")
	if models.DB.Model(&recipes).Where("id = ?", recipe_id).Updates(&input).RowsAffected == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Recipe update failed"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Recipe has been updated"})
}
