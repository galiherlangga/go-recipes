package middlewares

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/galiherlangga/recipes-api/utils/token"
)

func JwtAuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		err := token.TokenValid(ctx)
		if err != nil {
			ctx.String(http.StatusUnauthorized, "Unauthorized")
			ctx.Abort()
			return
		}
		ctx.Next()
	}
}
