package models

import "gorm.io/gorm"

type Recipes struct {
	gorm.Model
	Name   string `gorm:"size:255;not null;unique" json:"name"`
	UserID uint   `gorm:"not null;" json:"user_id"`
	User   User   `json:"user"`
}

func (r *Recipes) SaveRecipes() (*Recipes, error) {
	err := DB.Create(&r).Error
	if err != nil {
		return &Recipes{}, err
	}
	return r, nil
}
